import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraphscreenRoutingModule } from './graphscreen-routing.module';
import { GraphscreenComponent } from './graphscreen.component';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from '../components/header/header.component';
@NgModule({
    imports: [
        CommonModule,
        GraphscreenRoutingModule,
        Ng2Charts,
        NgbModule
    ],
    declarations: [GraphscreenComponent, HeaderComponent]
})
export class GraphModule {}
